FROM curlimages/curl AS builder

ENV SYNCTHING_VERSION=v1.27.2
ENV SYNCTHING_SHA256=fe645be8bc2c5e5ce6f8f17bc2e43b1654a5ce8822b605248cac2a57028652f4

RUN curl -L -o /tmp/syncthing.tar.gz "https://github.com/syncthing/syncthing/releases/download/${SYNCTHING_VERSION}/syncthing-linux-amd64-${SYNCTHING_VERSION}.tar.gz"
RUN echo test $SYNCTHING_SHA256 = $(sha256sum /tmp/syncthing.tar.gz | awk '{print $1}')
RUN tar -C /tmp -xzf /tmp/syncthing.tar.gz --strip-components 1

FROM cgr.dev/chainguard/static:latest

COPY --from=builder /tmp/syncthing /usr/local/bin/syncthing

CMD ["/usr/local/bin/syncthing"]
